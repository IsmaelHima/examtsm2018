# PART 4

import numpy as np
import pandas as pd
import os
import random
from random import randint
from random import choices
from Code.Part2  import Customer
#Import the Simulation Dataframe
SimulationDataframe = pd.read_csv("../Results/SimulationDat##The simulation dataset span 5 years.
##I'm going to generate a series of days ans their corresponding Total Cash Flow
# sum up all the total prices paid by customers at a given date, to get the Cash flow of this day
##For this, I use the function "groupby"  & I take the sum of elemnts in the column "S_TotalPrice"aFrame.csv", sep=',')

# 1. Show some buying histories of returning customers

#I will use a subsetting technique: get all the rows of my dataset set where the value in the column "S_CUSTOMER" is equal to a given ID

# a)for the customer with this ID: "RET[92]"
Example1OfBuyingHistory = SimulationDataframe[SimulationDataframe['S_CUSTOMER']=='RET[92]']
# print(Example1OfBuyingHistory)
##Save the subset
path = "../Results/Example1OfBuyingHistory.csv"
Example1OfBuyingHistory.to_csv(os.path.join(path))


# b)for the customer with this ID: "RET[948]"
Example2OfBuyingHistory = SimulationDataframe[SimulationDataframe['S_CUSTOMER']=='RET[948]']
# print(Example2OfBuyingHistory)
#save the subset
path = "../Results/Example2OfBuyingHistory.csv"
Example2OfBuyingHistory.to_csv(os.path.join(path))


# 2. In the dataset, there are actial returing customers.

# 2.1. How many?
#For the data set of the simulation
NumberOfOccurences = SimulationDataframe.S_CUSTOMER.value_counts()
print(NumberOfOccurences)
#The returning customers are those for whch the ID appears only more than one time in all the dataset
#We create a list of them from the dataset and print the length of the list
#The length of the list is the number of returning customers
ListOfReturningCustomers = NumberOfOccurences[NumberOfOccurences > 1].index.tolist()
print('The number of returning customers is %s' %str(len(ListOfReturningCustomers))) ## 1000 returning customers in the Dataframe of the simulation
                                                                                     #249680 One time customers
#==>The proportions of One Time & returning Customers are almost the same.

#2.2. Do they have specific time when they show up more?

##====>I USED THE FOLLOWING APPROACH TO ANSWER:
#I want a list of Date for which contains all the times at which we have a returning customer
TimesOfReturningCustomers = []

for row in SimulationDataframe.itertuples(index=False, name=None): #iterate this procedure over all the rows of the SimuationDataframe
    if row[1] in ListOfReturningCustomers:  #if the customer ID in the row is contained in the list of returning customers
        TimesOfReturningCustomers.append(row[5]) #record the time on this row in the list "TimesOfReturningCustomers"

print(len(TimesOfReturningCustomers)) # => There are 62395 rows where it has been a returning customer purchasing an item.
 #I import the function Counter from the library collections to count the numbr of occurences of each element in the list
from collections import Counter
#I apply it on my list
count=Counter(TimesOfReturningCustomers)
#I select the most 20 most common element in my list of times
print(count.most_common(20))
##==> Out of the 20 elements, 13 elements are times in the afternoon,
# The retuning customers seems to show up more in the afternoon

# 2.2. The probability of having a returning customer

TotalNumberOfTimes = 312074  #The total number of time when there is a purchase=Total number of the rows of the dataset

##ProbaReturning =Proba to have a One Time Customer
# ProbaReturning = (Number of rows that contains a returning customer / Total number of rows)
print(1%3)
print('The probability to have a Returningtime customer at a given time is: %s' %str(float(62395 /312074)))
##  ==>ProbaReturning= 0.19993655350974449 ~20%

# 2.3. How does this impact their buying history?
# ==> As the proba of having a returning customer at a given time is very low, there will be less returning customers in the dataset,

# ==> I don't see correlation

# 2.4.What would happen if we lower the remaining customers to 50 and simulate the same period?
#==> If we maintain, to initial probailities of 80% and 20%, the number of items purchased by each of the returnig customers will necerrarily increase

# 2.5.The price change from the beginning of 2015 and go up by 20%
# ==> There will be more customers dropped from the list of the returning customers, as their budget will lower more quickly

# Note: in Part 3, it was said:
#  ' IF A RETURNING CUSTOMER IS NO LONGER ABLE TO BUY THE MOST EXPENSIVE FOOD AND DRINK, IT SHOULD NO LONGER BE POSSIBLE TO RANDOMLY DRAW HIM/HER
#   FROM THE POOL OF 1000 RETURNING CUSTOMERS'
# 2.6. The budget of hipsters drops to 40:
#==> There will be more hipsters dropped from the list of the returning ucustomers as their budget will lower quickly



