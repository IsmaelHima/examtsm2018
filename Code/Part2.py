#PART 2
import numpy as np
import pandas as pd
import os
import random
from random import randint
from random import choices   #Source: https://stackoverflow.com/questions/4265988/generate-random-numbers-with-a-given-numerical-distribution

#For this part, I first import all the necessary dataframes:

#-The initial dataframe where the time has been converted to type "datetime" and with all the missig values replaced by the string "NoFood"
NewDataframe = pd.read_csv("../Results/NewDataframe.csv", sep=',')
#- The dataframe that contains the probabilities from PART1
##-For DRINKS
DrinkProbaDataframe = pd.read_csv("../Results/DrinkProbaDataframe.csv", sep=',')
##-For FOOD
FoodProbaDataframe = pd.read_csv("../Results/FoodProbaDataframe.csv", sep=',')


#I want to create a class named Customer that will have the following attributes:
#-ID : The unique customer number
#-Frequency: 2 possible values: "OneTimeCustomer" or "ReturningCustomer"
#Type : 2 possible values depending on the frequency:
    # for a customer with Frequency="OneTimeCustomer": "Regular" or "FromTriAdvisor"
    # for a customer with Frequency="ReturningCustomer": "Regular" or "Hipster"
#An attribute for the budget of the customer: "Budget"
#An attribute for the History of the customer("HistoryOfPurchases"): this will be by default an empty list composed of 4 empty sublist
    #In its 1st sublist: there will be recorded the "time" (in the format"Hour:Minute") at which the customer buys
    #In the 2nd sublist: there will be recorded the "Drink" purchased at this time
    #In the 3rd sublist: there will be recorded the "Food" purchased at this time
    #In the 4th sublist: there will be recorded the "TotalPrice" paid for the 1 or 2 items purchased


class Customer(object):
    def __init__(self, ID,Frequency,Type,Budget):
        self.ID = ID
        self.Frequency= Frequency
        self.Type=Type
        self.Budget = Budget
        self.HistoryOfPurchases = [[], [], [], []]

    # Define a method to draw randomly a drink purchased at a given time:
    #It will randomly generate random drinks with a given (numerical) distribution
    #a solution for this in Python's standard library, namely random.choices.
    def DrinkPurchased(self, TimeOfPurchase):  # the argument of this method is Time
        pos = DrinkProbaDataframe.set_index('TIME').index.get_loc(TimeOfPurchase) #This corresponds to the index of the "Time" used as the argument in the dataframe "DrinkProbaDataframe"
        probacoffee = DrinkProbaDataframe.loc[pos, 'coffee']  #This corresponds to the element corresponding to this index in the column 'coffee" i.e: the probability for a customer to buy coffee at this time
        probafrappucino = DrinkProbaDataframe.loc[pos, 'frappucino'] # Idem for the column "frappucino"
        probamilkshake = DrinkProbaDataframe.loc[pos, 'milkshake']  #And I continue to take all the elements on the row corresponding to this index
        probasoda = DrinkProbaDataframe.loc[pos, 'soda']
        probatea = DrinkProbaDataframe.loc[pos, 'tea']
        probawater = DrinkProbaDataframe.loc[pos, 'water']
        Drinks = ['coffee', 'frappucino', 'milkshake', 'soda', 'tea', 'water'] #I create a list of the drinks
        DrinksProbas = [probacoffee, probafrappucino, probamilkshake, probasoda, probatea, probawater] #I put all the above probabilities in a list in the same order as in the list of the Drinks
        RandomDrink = choices(Drinks, DrinksProbas)  #I use the function "choices"
        return RandomDrink

    # Define a method to draw randomly a food purchased at a given time:
    #I perfom the same procedure to generate random food with a given(numerical) distribution
    def FoodPurchased(self, TimeOfPurchase):  # the argument is Time
        pos = FoodProbaDataframe.set_index('TIME').index.get_loc(TimeOfPurchase)
        probaNoFood = FoodProbaDataframe.loc[pos, 'NoFood']
        probacookie = FoodProbaDataframe.loc[pos, 'cookie']
        probamuffin = FoodProbaDataframe.loc[pos, 'muffin']
        probapie = FoodProbaDataframe.loc[pos, 'pie']
        probasandwich = FoodProbaDataframe.loc[pos, 'sandwich']
        Food = ['NoFood', 'cookie', 'muffin', 'pie', 'sandwich']
        FoodProbas = [probaNoFood, probacookie, probamuffin, probapie, probasandwich]
        RandomDrink = choices(Food, FoodProbas)
        return RandomDrink

    # I define a method to determine the Amount Paid for drink at any time:
    def AmountPaidforDrink(self, drink):
        if drink == ['coffee']:
             price = 3
        elif drink == ['milkshake']:
             price = 5
        elif drink == ['frappucino']:
             price = 4
        elif drink == ['soda']:
             price = 3
        elif drink == ['tea']:
             price = 3
        else:
             price = 2
        return price

    # I define a method to determine the Amount Paid for Food at any time
    def AmountPaidforFood(self, food):
        # food = self.FoodPurchased(TimeOfPurchase)
        if food == ['sandwich']:
            price = 5
        elif food == ['muffin']:
            price = 3
        elif food == ['pie']:
            price = 3
        elif food == ['cookie']:
            price = 2
        else:
            price = 0
        return price

    #I define a method to determine the Total amount Paid at any time
    def TotalAmountPaid(self, drink, food):
        if self.Type == 'FromTripAdvisor':
            TotalPrice = self.AmountPaidforDrink(drink) + self.AmountPaidforFood(food)+randint(1,10)
        else:
            TotalPrice = self.AmountPaidforDrink(drink) + self.AmountPaidforFood(food)
        return TotalPrice

#I Define a method which operate a transaction and all related operations
    # For a OneTimeCustomer,the method will do the same thing except for the Total price in the case it has type "FromTripAdvisor"

    def Transaction(self, TimeOfPurchase):
        self.HistoryOfPurchases[0].append(TimeOfPurchase) #The time at which the transaction is effected IS RECORDED in the 1st sublist of the list "HistoryOfPurchases"
        Drink = self.DrinkPurchased(TimeOfPurchase)  # A random drink is generated
        self.HistoryOfPurchases[1].append(Drink) ##The drink purchased IS RECORDED in the 2nd sublist of the list "HistoryOfPurchases"
        Food = self.FoodPurchased(TimeOfPurchase) # A random food is generated
        self.HistoryOfPurchases[2].append(Food)  ##The drink purchased IS RECORDED in the 3rd sublist of the list "HistoryOfPurchases"
        TotalPrice = self.TotalAmountPaid(Drink, Food) #The Total Price paid for the random drink and food is calculated
        self.HistoryOfPurchases[3].append(TotalPrice) #This total price IS RECORDED in the 4th sublist of the list "HistoryOfPurchases"
        self.Budget += - TotalPrice # The budget is updated by subtracting the TotalPrice


# Let's test our code

#Create a customer with Frequency="OneTimeCustomer" & Type = "FromTripAdvisor"
James = Customer('012358862','OneTimeCustomer','FromTripAdvisor',100)
print(James.Budget)

#1st Transaction
James.Transaction('08:00')
#check the history
print(James.HistoryOfPurchases)
# check the remaining budget
print(James.Budget)

#2nd transaction
James.Transaction('15:48')
#check the history
print(James.HistoryOfPurchases)
# check the remaining budget
print(James.Budget)
